import cv2
from matplotlib import pyplot as plt
import numpy as np
import sys

image = cv2.imread("img.jpg");
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
height = len(image);
width = len(image[0]);


px = np.zeros((256), dtype=int);

for i in range(height):
    for j in range(width):
        px[image[i][j]] = px[image[i][j]] + 1;

print(px);

cdf = np.zeros(256, dtype=int);

for i in range(256):
    for j in range (i+1):
        cdf[i] = cdf[i] + px[j];

print(cdf)

cdfmin = 0
for i in range(256):
    if cdf[i] != 0:
        cdfmin = cdf[i]
        break

h = np.zeros(256, dtype = float);

for i in range(256):
    value = (cdf[i]-cdfmin) * 255
    value /= (height * width - 1)
    h[i] = round(value)

for i in range(height):
    for j in range(width):
        image[i][j] = h[image[i][j]]

cv2.imwrite('eqimg.png',image)